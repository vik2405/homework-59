import React, { Component } from 'react';
import './App.css';
import Post from "./Post";

class App extends Component {


  state = {
    post: ' '
};
  getPost = () => {
    fetch('https://api.chucknorris.io/jokes/random').then(response => {
      if (response.ok) {
        return response.json();
      }
      throw new Error('Something went wrong');
    }).then(joke => {
      console.log(joke);
      this.setState({post: joke.value})
    })
  };

  componentDidMount() {
    this.getPost();
  }


  render() {
    return (
      <div className="App">
        <h1>Jokes from Chuck Norris !!!!</h1>
        <Post text={this.state.post}/>
        <button className="btn" onClick={this.getPost}>ENTER</button>
      </div>
    );
  }
}

export default App;
