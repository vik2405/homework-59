import React from 'react';
import  './Post.css';


const Post = (props) => {
    return (
        <p className="post">{props.text}</p>
    )
};

export default Post;